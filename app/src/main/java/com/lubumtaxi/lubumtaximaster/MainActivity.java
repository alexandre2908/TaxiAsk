package com.lubumtaxi.lubumtaximaster;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.lubumtaxi.lubumtaximaster.Common.Common;
import com.lubumtaxi.lubumtaximaster.Models.User;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    Button btn_login,btn_register;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference users;
    RelativeLayout relativeLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig
                .Builder().setDefaultFontPath("fonts/Arkhip_font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_main);

        //init firebase
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        users = firebaseDatabase.getReference(Common.user_driver_tbl);
        //event
        btn_login = (Button)findViewById(R.id.btn_Login);
        //btn_register =(Button)findViewById(R.id.btn_register);
        relativeLayout = (RelativeLayout) findViewById(R.id.rootLayout);

        //init views

       /* btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegisterDialog();
            }
        });*/

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLoginDialog();
            }
        });

    }

    private void showLoginDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle("Connection");
        dialog.setMessage("utilisez votre adresse email");
        LayoutInflater inflater = LayoutInflater.from(this);
        final View login_layout = inflater.inflate(R.layout.layout_login,null);
        final MaterialEditText txt_email = login_layout.findViewById(R.id.txt_email);
        final MaterialEditText txt_password = login_layout.findViewById(R.id.txt_password);
        dialog.setView(login_layout);

        //set button

        dialog.setPositiveButton("Se Connecter", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                //check validation
                btn_login.setEnabled(false);
                if (TextUtils.isEmpty(txt_email.getText().toString())) {
                    Snackbar.make(login_layout, "veuillez entrer l'adresse email", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(txt_password.getText().toString())) {
                    Snackbar.make(login_layout, "veuillez entrer le mots de passe", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                if (txt_password.getText().toString().length() <6) {
                    Snackbar.make(login_layout, "veuillez entrer le mots de passe a au moins 6 characteres", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                final SpotsDialog waitingdialogue = new SpotsDialog(MainActivity.this);
                waitingdialogue.show();
                // login process

                firebaseAuth.signInWithEmailAndPassword(txt_email.getText().toString(), txt_password.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        waitingdialogue.dismiss();
                        FirebaseDatabase.getInstance().getReference(Common.user_driver_tbl)
                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Common.currentUser = dataSnapshot.getValue(User.class);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                        startActivity( new Intent(MainActivity.this, Welcome.class));
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        waitingdialogue.dismiss();
                        Snackbar.make(relativeLayout, "Failed "+e.getMessage(), Snackbar.LENGTH_SHORT).show();

                        btn_login.setEnabled(true);
                    }
                });
            }
        });

        dialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog.show();
    }
    private void showRegisterDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle("Enregistrement");
        dialog.setMessage("utilisez votre adresse email");
        LayoutInflater inflater = LayoutInflater.from(this);
        final View register_layout = inflater.inflate(R.layout.layout_register,null);
        final MaterialEditText txt_email = register_layout.findViewById(R.id.txt_email);
        final MaterialEditText txt_name = register_layout.findViewById(R.id.txt_name);
        final MaterialEditText txt_password = register_layout.findViewById(R.id.txt_password);
        final MaterialEditText txt_phone = register_layout.findViewById(R.id.txt_phone);
        dialog.setView(register_layout);

        //set button

        dialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog.setPositiveButton("Enregistrer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                //check validation
                if (TextUtils.isEmpty(txt_email.getText().toString())){
                    Snackbar.make(relativeLayout, "veuillez completer l'email",Snackbar.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(txt_name.getText().toString())){
                    Snackbar.make(relativeLayout, "veuillez completer le nom",Snackbar.LENGTH_SHORT).show();

                    return;
                }
                if (TextUtils.isEmpty(txt_password.getText().toString())){
                    Snackbar.make(relativeLayout, "veuillez completer le mots de passe",Snackbar.LENGTH_SHORT).show();

                    return;
                }
                if (txt_phone.getText().toString().length() <6){
                    Snackbar.make(relativeLayout, "mots de passe tres cours",Snackbar.LENGTH_SHORT).show();

                    return;
                }
                firebaseAuth.createUserWithEmailAndPassword(txt_email.getText().toString(),txt_password.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        User user = new User();
                        user.setEmail(txt_email.getText().toString());
                        user.setName(txt_name.getText().toString());
                        user.setPassword(txt_password.getText().toString());
                        user.setPhone(txt_phone.getText().toString());
                        users.child(firebaseAuth.getCurrentUser().getUid()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Snackbar.make(relativeLayout, "Enregistrement reussi",Snackbar.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Snackbar.make(relativeLayout, "Enregistrement echoue car "+e.getMessage(),Snackbar.LENGTH_SHORT).show();


                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(relativeLayout, "Enregistrement echoue car "+e.getMessage(),Snackbar.LENGTH_SHORT).show();
                    }
                });

            }
        });

        dialog.show();
    }

}

