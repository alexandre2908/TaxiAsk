package com.lubumtaxi.lubumtaximaster.Remote;

import com.lubumtaxi.lubumtaximaster.Models.FCMResponse;
import com.lubumtaxi.lubumtaximaster.Models.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMService {
    @Headers({
            "Content-type:application/json",
            "Authorization:key=AAAAQbJRvnk:APA91bHxJKNc1EJq11qJHYlhZYXr-_5USLBvn4RCh9JKfhxZV_jZ6gt10cr3fm7cr4bj9bx6_77C-_-eX5Nj3lTd0APsKN6SfVYMjhWNX7n0yExgUlbhu1tP1bsdLlE37rVcn2a6FctfgbkQtYsKwVQ6i9cfyp4tMA"
    })
    @POST("fcm/send")
    Call<FCMResponse> sendMessage(@Body Sender body);
}
