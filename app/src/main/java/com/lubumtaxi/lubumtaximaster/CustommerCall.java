package com.lubumtaxi.lubumtaximaster;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.lubumtaxi.lubumtaximaster.Common.Common;
import com.lubumtaxi.lubumtaximaster.Models.Commandes;
import com.lubumtaxi.lubumtaximaster.Models.FCMResponse;
import com.lubumtaxi.lubumtaximaster.Models.Notification;
import com.lubumtaxi.lubumtaximaster.Models.Sender;
import com.lubumtaxi.lubumtaximaster.Models.Token;
import com.lubumtaxi.lubumtaximaster.Remote.IFCMService;
import com.lubumtaxi.lubumtaximaster.Remote.IGoogleAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustommerCall extends AppCompatActivity {
    TextView txtTime, txtAdresse, txtDistance;
    MediaPlayer mediaPlayer;
    Button btn_accepter, btn_refuser;
    DatabaseReference mDatabase;

    IGoogleAPI mService;
    IFCMService mFCMService;
    String customerId;
    double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custommer_call);
        mService = Common.getGoogleAPI();
        mFCMService = Common.getFCMService();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        //init view

        txtAdresse = (TextView) findViewById(R.id.txtAdresse);
        txtTime = (TextView) findViewById(R.id.txtTime);
        txtDistance = (TextView) findViewById(R.id.txtDisatance);
        btn_accepter = (Button) findViewById(R.id.btn_accepter) ;
        btn_refuser = (Button) findViewById(R.id.btn_refuser) ;

        btn_refuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(customerId))
                    cancelBooking(customerId);
            }
        });
        btn_accepter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustommerCall.this, DriverTracking.class);
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("customerId", customerId);
                startActivity(intent);
                finish();
            }
        });
        mediaPlayer = MediaPlayer.create(this, R.raw.ringtone);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        if (getIntent() != null){
            lat = getIntent().getDoubleExtra("lat", -1.0);
            lng = getIntent().getDoubleExtra("lng", -1.0);
            customerId = getIntent().getStringExtra("customer");
            getDirection(lat, lng);
        }

        writeNewPost(customerId, Common.currentUser.getEmail());

    }

    private void writeNewPost(String customer_id, String driver_email) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("rapports").push().getKey();
        Commandes commandes = new Commandes(customer_id, driver_email);
        Map<String, Object> postValues = commandes.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/rapports/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void cancelBooking(String customerId) {
        Token token = new Token(customerId);
        Notification notification = new Notification("Cancel", "Le chauffeur a refuse votre demande");
        Sender sender = new Sender(token.getToken(), notification);
        mFCMService.sendMessage(sender).enqueue(new Callback<FCMResponse>() {
            @Override
            public void onResponse(Call<FCMResponse> call, Response<FCMResponse> response) {
                if (response.body().success == 1) {
                    Toast.makeText(CustommerCall.this, "Annullee", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<FCMResponse> call, Throwable t) {

            }
        });
    }

    private void getDirection(double lat, double lng) {
        String requestApi = null;

        try {
            requestApi = "https://maps.googleapis.com/maps/api/directions/json?"+
                    "mode=driving&"+
                    "transit_routing_preference=less_driving&"+
                    "origin="+ Common.LastLocation.getLatitude()+","+Common.LastLocation.getLongitude()+"&"+
                    "destination="+lat+","+lng+"&"+
                    "key="+getResources().getString(R.string.google_direction_api);

            Log.d("lubumtaxi",requestApi);

            mService.getPath(requestApi)
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response.body().toString());

                               JSONArray routes = jsonObject.getJSONArray("routes");

                                //recuperation du premier element
                                JSONObject object = routes.getJSONObject(0);

                                JSONArray legs = object.getJSONArray("legs");

                                JSONObject legsObject = legs.getJSONObject(0);

                                //now get distance

                                JSONObject distance = legsObject.getJSONObject("distance");
                                txtDistance.setText(distance.getString("text"));

                                //get time
                                JSONObject time = legsObject.getJSONObject("duration");
                                txtTime.setText(time.getString("text"));

                                //get adresse

                                String adresse = legsObject.getString("end_address");
                                txtAdresse.setText(adresse);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(CustommerCall.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        mediaPlayer.release();
        super.onStop();
    }

    @Override
    protected void onPause() {
        mediaPlayer.release();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer.start();
    }
}
