# Taxi Web Driver

This is an Uber clone project for the Drivers helping them to: 
    
  - Create and account and loggin
  - Handle for a command
  - Accept or Cancel the booking
  - If accept the app will draw a road to the client and estimate the time
  - Once arrive send a notification
  - etc
  

# Future Features!

  - Adding more control for drivers
  - Improve UI and UX

# Getting Started

Just clone the project using
```sh
git clone https://gitlab.com/alexandre2908/TaxiAsk.git
```
and then open it in Android studio and then customize it as you want


Here is the client app : 

```sh 
git clone https://gitlab.com/alexandre2908/TaxiAsk_Client.gi
```


# Prerequisite
- Advanced Knowledge of java and mobile developpement
- Advanced Knowledge of google map 

### Author


* [Axel Mwenze](https:axelmwenze.com) - Software Engineer and Fullstack Web Developper


License
----

MIT


**Free Software, Hell Yeah!**
